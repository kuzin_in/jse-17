package ru.kuzin.tm.command.task;

import ru.kuzin.tm.enumerated.Status;
import ru.kuzin.tm.util.TerminalUtil;

public final class TaskStartByIdCommand extends AbstractTaskCommand {

    private static final String NAME = "task-start-by-id";

    private static final String DESCRIPTION = "Start task by id.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        getTaskService().changeTaskStatusById(id, Status.IN_PROGRESS);
    }

}